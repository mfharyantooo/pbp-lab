from django.db import models

class Note(models.Model):
    note_to = models.CharField(max_length=30)
    note_from = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    message = models.TextField()
    
