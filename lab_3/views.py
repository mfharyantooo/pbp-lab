from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .forms import FriendForm
from lab_1.models import Friend

@login_required(login_url='/admin/login/')
def index(request):
    friend_list = Friend.objects.all()
    response = {'friend_list': friend_list}

    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm(request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect('/lab-3')

    response = {'form': form}
    
    return render(request, 'lab3_form.html', response)
