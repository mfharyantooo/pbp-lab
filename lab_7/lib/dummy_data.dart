import 'package:lab_7/models/kegiatan.dart';
import 'package:lab_7/models/rekomendasi.dart';

// ignore: constant_identifier_names
const DUMMY_KEGIATAN = [
  Kegiatan(id: '1', nama: 'Bermain game', deskripsi: 'Valorant'),
  Kegiatan(id: '2', nama: 'Menonton film', deskripsi: 'Eternals'),
  Kegiatan(id: '3', nama: 'Memasak', deskripsi: 'Memasak bersama Ibu'),
  Kegiatan(id: '4', nama: 'Olahraga', deskripsi: 'Lari pagi bersama adik'),
  Kegiatan(id: '5', nama: 'Menonton film', deskripsi: 'Rewatch Kimi no Na Wa'),
  Kegiatan(id: '6', nama: 'Mendengarkan musik', deskripsi: 'OST Hospital Playlist'),
];

// ignore: constant_identifier_names
const DUMMY_REKOMENDASI = [
  Rekomendasi(id: '1', nama: 'Bermain game'),
  Rekomendasi(id: '2', nama: 'Menonton film'),
  Rekomendasi(id: '3', nama: 'Membaca buku'),
  Rekomendasi(id: '4', nama: 'Mendengarkan musik'),
  Rekomendasi(id: '5', nama: 'Belanja'),
  Rekomendasi(id: '6', nama: 'Mengunjungi tempat wisata'),
  Rekomendasi(id: '7', nama: 'Olahraga'),
  Rekomendasi(id: '8', nama: 'Menulis'),
  Rekomendasi(id: '9', nama: 'Memasak'),
  Rekomendasi(id: '10', nama: 'Membersihkan rumah'),
  Rekomendasi(id: '11', nama: 'Karaoke'),
];
