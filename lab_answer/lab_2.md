### HTML, XML, dan JSON

1. Apakah perbedaan antara JSON dan XML?
- JSON merupakan format pertukaran file yang ditulis dalam Javascript sedangkan XML adalah bahasa markup.
- Data dalam JSON disimpan sebagai pasangan key dan value sedangkan data dalam XML disimpan dalam struktur tree.
- JSON mendukung array sedangkan XML tidak mendukung array secara langsung.
- JSON lebih ringkas, sederhana dan mudah dibaca karena tidak menggunakan tag sedangkan XML lebih besar dan rumit untuk dibaca karena menggunakan tag.
- JSON hanya mendukung beberapa tipe data sedangkan XML mendukung tipe data yang lebih banyak dan kompleks dibandingkan JSON.
- JSON tidak mendukung komentar sedangkan XML mendukung komentar.
- JSON tidak mendukung namespaces sedangkan XML mendukung namespaces.
- JSON cenderung kurang aman dibandingkan XML.

2. Apakah perbedaan antara HTML dan XML?
- HTML berfokus pada penyajian data sedangkan XML berfokus pada pengangkutan data.
- HTML case insensitive sedangkan XML case sensitive.
- HTML memiliki tag yang telah ditentukan sedangkan tag pada XML tidak ditentukan sebelumnya.
- Tag pada HTML terbatas sedangkan tag pada XML lebih dinamis.
- Beberapa elemen HTML tidak memerlukan tag penutup sedangkan semua elemen di XML harus menggunakan tag penutup.
- HTML tidak mendukung konsep namespaces sedangkan XML mendukung namespaces.
- HTML mengabaikan kesalahan kecil sedangkan XML tidak memperbolehkan adanya kesalahan sama sekali.
