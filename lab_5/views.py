from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views.generic import View
from .forms import NoteForm
from lab_2.models import Note

def index(request):
    note_list = Note.objects.all()
    response = {'note_list': note_list,
            }
    return render(request, 'lab5_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)

    if request.method == "POST" and form.is_valid():
        form.save()
        return redirect('/lab-5/')

    response = {'form': form}
    
    return render(request, 'lab5_form.html', response)

def note_list(request):
    note_list = Note.objects.all()
    response = {'note_list': note_list,
            }
    return render(request, 'lab5_note_list.html', response)

class EditNote(View):
    def get(self, request):
        id1 = request.GET.get('id', None)
        title1 = request.GET.get('title', None)
        note_from1 = request.GET.get('note_from', None)
        note_to1 = request.GET.get('note_to', None)
        message1 = request.GET.get('message', None)

        obj = Note.objects.get(id=id1)
        obj.title = title1
        obj.note_from = note_from1
        obj.note_to = note_to1
        obj.message = message1

        note = {
            'id':obj.id,
            'title':obj.title,
            'note_from':obj.note_from,
            'note_to':obj.note_to,
            'message':obj.message    
        }

        data = {
            'note': note
        }
        return JsonResponse(data)

class DeleteNote(View):
    def  get(self, request):
        id1 = request.GET.get('id', None)
        Note.objects.get(id=id1).delete()
        data = {
            'deleted': True
        }
        return JsonResponse(data)