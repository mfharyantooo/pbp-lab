class Rekomendasi {
  final String id;
  final String nama;

  const Rekomendasi({
    required this.id,
    required this.nama,
  });
}
