import 'package:flutter/material.dart';

class AddRekomendasiKegiatanPage extends StatefulWidget {
  const AddRekomendasiKegiatanPage({Key? key}) : super(key: key);
  @override
  State<AddRekomendasiKegiatanPage> createState() => _RefleksiHomePageState();
}

class _RefleksiHomePageState extends State<AddRekomendasiKegiatanPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Tambah Kegiatan'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              color: Colors.grey[200],
              height: 300,
              child: Stack(
                children: <Widget>[
                  const Positioned(
                    left: 25,
                    top: 20,
                    right: 25,
                    child: Text('Silakan tambahkan rekomendasi kegiatanmu',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 22)),
                  ),
                  Positioned(
                    left: 15,
                    top: 90,
                    right: 15,
                    bottom: 15,
                    child: Container(
                      color: Colors.grey[300],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
